import { Component, OnInit } from "@angular/core";
import { ProductService } from "../services/product.service";
import { Product } from "../models/Product";

@Component({
  selector: "app-all-products",
  templateUrl: "./all-products.component.html",
  styleUrls: ["./all-products.component.scss"]
})
export class AllProductsComponent implements OnInit {
  products: Product[];

  constructor(private produtService: ProductService) {}

  ngOnInit() {
    // this.produtService.getProducts().subscribe(data => {
    //   this.products = data;
    //   console.log(data);
    // });
  }
}
