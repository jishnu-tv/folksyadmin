import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { AngularFireAuth } from "@angular/fire/auth";
import {
  AngularFirestore,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
import { Seller } from "../models/Seller";
import { Admin } from "../models/Admin";
import { Router } from "@angular/router";
import { throwError, of } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  newSeller: Seller;
  newAdmin: Admin;
  isSeller: string;
  isAdmin: string;

  sellerDoc: AngularFirestoreDocument<Seller>;
  seller: any;

  adminDoc: AngularFirestoreDocument<Admin>;
  admin: any;

  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private router: Router,
    private afs: AngularFirestore
  ) {}

  // Register new seller
  createSeller(seller) {
    this.afAuth.auth
      .createUserWithEmailAndPassword(seller.email, seller.password)
      .then(sellerCredential => {
        this.newSeller = seller;
        this.insertSellerData(sellerCredential).then(() => {
          this.router.navigate(["/signin"]);
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  // Add seller details
  insertSellerData(sellerCredential: firebase.auth.UserCredential) {
    return this.db.doc(`seller/${sellerCredential.user.uid}`).set({
      firstName: this.newSeller.firstName,
      lastName: this.newSeller.lastName,
      storeName: this.newSeller.storeName,
      email: this.newSeller.email,
      addressLineOne: this.newSeller.addressLineOne,
      addressLineTwo: this.newSeller.addressLineTwo,
      pincode: this.newSeller.pincode,
      country: this.newSeller.country,
      state: this.newSeller.state,
      city: this.newSeller.city,
      phoneNumber: this.newSeller.phoneNumber,
      aboutYou: this.newSeller.aboutYou,
      role: "seller",
      createdAt: new Date(),
      updatedAt: new Date()
    });
  }

  // Register admin
  createAdmin(admin) {
    this.afAuth.auth
      .createUserWithEmailAndPassword(admin.email, admin.password)
      .then(adminCredential => {
        this.newAdmin = admin;
        this.insertAdminData(adminCredential).then(() => {
          this.router.navigate(["/signin"]);
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  // Add admin details
  insertAdminData(adminCredential: firebase.auth.UserCredential) {
    return this.db.doc(`admin/${adminCredential.user.uid}`).set({
      firstName: this.newAdmin.firstName,
      lastName: this.newAdmin.lastName,
      email: this.newAdmin.email,
      addressLineOne: this.newAdmin.addressLineOne,
      addressLineTwo: this.newAdmin.addressLineTwo,
      pincode: this.newAdmin.pincode,
      country: this.newAdmin.country,
      state: this.newAdmin.state,
      city: this.newAdmin.city,
      phoneNumber: this.newAdmin.phoneNumber,
      role: "admin",
      createdAt: new Date(),
      updatedAt: new Date()
    });
  }

  // Login Seller
  loginSeller(email: string, password: string) {
    this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(sellerCredential => {
        if (sellerCredential) {
          this.sellerDoc = this.afs.doc<Seller>(
            "seller/" + sellerCredential.user.uid
          );
          this.seller = this.sellerDoc.valueChanges().subscribe(seller => {
            if (seller.role === "seller") {
              this.router.navigate(["/seller/" + sellerCredential.user.uid]);
            } else {
              console.log("Please signin with your seller account");
            }
          });
        }
      })
      .catch(error => {
        return error;
      });
  }

  // Login Admin
  loginAdmin(email: string, password: string) {
    this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(adminCredential => {
        if (adminCredential) {
          this.adminDoc = this.afs.doc<Seller>(
            "admin/" + adminCredential.user.uid
          );
          this.admin = this.adminDoc.valueChanges().subscribe(admin => {
            if (admin.role === "admin") {
              this.router.navigate(["/" + adminCredential.user.uid]);
            } else {
              console.log("Please signin with your admin account");
            }
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  // Check auth
  get getAuth() {
    return this.afAuth.authState.pipe(map(auth => auth));
  }

  // Logout
  logout() {
    this.afAuth.auth
      .signOut()
      .then(() => {
        console.log("Logout sucessfully");
      })
      .catch(error => {
        console.log(error);
      });
  }
}
