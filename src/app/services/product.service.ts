import { Injectable } from "@angular/core";
import { Product } from "../models/Product";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class ProductService {
  productsCollection: AngularFirestoreCollection<Product>;
  productDoc: AngularFirestoreDocument<Product>;
  products: Observable<Product[]>;
  product: Observable<Product>;

  constructor(private afs: AngularFirestore) {}

  get timeStamp() {
    let date = new Date();
    return date;
  }

  addData(data: Product) {
    return this.afs.collection("products").add(data);
  }

  getProducts(uid) {
    this.afs
      .collection("products")
      .doc("sellers")
      .collection(uid)
      .snapshotChanges()
      .pipe(
        map(a => {
          a.map(b => {
            const data = b.payload.doc.data() as Product;
            const id = b.payload.doc.id;
            return { id, ...data };
          });
        })
      );
  }

  getProduct(id: string, uid) {
    this.productDoc = this.afs
      .doc("sellers")
      .collection(uid)
      .doc<Product>(uid + "/" + id);
    return (this.product = this.productDoc.valueChanges());
  }
}
