import { TestBed } from '@angular/core/testing';

import { SigninSellerGaurdService } from './signin-seller-gaurd.service';

describe('SigninSellerGaurdService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SigninSellerGaurdService = TestBed.get(SigninSellerGaurdService);
    expect(service).toBeTruthy();
  });
});
