import { TestBed } from '@angular/core/testing';

import { AuthSellerGaurdService } from './auth-seller-gaurd.service';

describe('AuthSellerGaurdService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthSellerGaurdService = TestBed.get(AuthSellerGaurdService);
    expect(service).toBeTruthy();
  });
});
