import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Router } from "@angular/router";
import { CanActivate } from "@angular/router/src/utils/preactivation";
import { map } from "rxjs/operators";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestoreDocument } from "@angular/fire/firestore";
import { Seller } from "../../models/Seller";

@Injectable({
  providedIn: "root"
})
export class SigninSellerGaurdService implements CanActivate {
  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;
  sellerDoc: AngularFirestoreDocument<Seller>;
  seller: any;

  constructor(private afAuth: AngularFireAuth) {}

  canActivate() {
    return this.afAuth.authState.pipe(
      map(auth => {
        if (!auth) {
          return false;
        } else {
          return true;
        }
      })
    );
  }
}
