import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { Signin } from "../models/Signin";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.component.html",
  styleUrls: ["./signin.component.scss"]
})
export class SigninComponent implements OnInit {
  user: any;
  constructor(private auth: AuthService) {}

  spinner: boolean = false;

  login(seller: Signin) {
    this.spinner = true;
    this.auth.loginSeller(seller.value.email, seller.value.password);
  }

  ngOnInit() {
    if (this.spinner == true) {
      this.auth.getAuth.subscribe(() => (this.spinner = false));
    } else {
      this.spinner = false;
    }
  }
}
