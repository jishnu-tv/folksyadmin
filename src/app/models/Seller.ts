export interface Seller {
  firstName: String;
  lastName: String;
  storeName: String;
  email: String;
  addressLineOne: String;
  addressLineTwo: String;
  pincode: Number;
  country: String;
  state: String;
  city: String;
  password: String;
  phoneNumber: Number;
  aboutYou: String;
  role: String;
  createdAt: Date;
  updatedAt: Date;
}
