export interface Admin {
  firstName: String;
  lastName: String;
  email: String;
  addressLineOne: String;
  addressLineTwo: String;
  pincode: Number;
  country: String;
  state: String;
  city: String;
  password: String;
  phoneNumber: Number;
  role: String;
  createdAt: Date;
  updatedAt: Date;
}
