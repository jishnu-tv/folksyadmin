export interface Product {
  productName: string;
  shortName: string;
  longName: string;
  tags: string;
  category: string;
  subCategory: string;
  stock: number;
  cod: boolean;
  onOffer: boolean;
  mrp: number;
  currentPrice: number;
  highlights: string;
  description: string;
  createdAt: any;
  updatedAt: any;
  sellerId: string;
}
