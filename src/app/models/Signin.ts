export interface Signin {
  value: {
    email: string;
    password: string;
  };
}
