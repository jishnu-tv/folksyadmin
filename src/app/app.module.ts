import { BrowserModule } from "@angular/platform-browser";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { MDBBootstrapModule } from "angular-bootstrap-md";
import { FormsModule } from "@angular/forms";

import { AngularFireModule } from "angularfire2";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireStorageModule } from "angularfire2/storage";
import { AngularFireAuthModule, AngularFireAuth } from "@angular/fire/auth";
import { environment } from "../environments/environment";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { AddProductComponent } from "./seller/add-product/add-product.component";
import { UpdateProductComponent } from "./seller/update-product/update-product.component";
import { FooterComponent } from "./footer/footer.component";
import { TagInputModule } from "ngx-chips";
import { UsersComponent } from "./users/users.component";
import { SellersComponent } from "./sellers/sellers.component";
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { SigninComponent } from "./signin/signin.component";
import { SignupComponent } from "./signup/signup.component";
import { ContactComponent } from "./contact/contact.component";
import { PrivacyComponent } from "./privacy/privacy.component";
import { TAndCComponent } from "./t-and-c/t-and-c.component";
import { OrdersComponent } from "./seller/orders/orders.component";
import { UserDetailsComponent } from "./user-details/user-details.component";
import { SellerDetailsComponent } from "./seller-details/seller-details.component";
import { AllProductsComponent } from "./all-products/all-products.component";
import { ProductDetailsComponent } from "./seller/product-details/product-details.component";
import { RegisterSellerComponent } from "./seller/register-seller/register-seller.component";
import { SellerDashboardComponent } from "./seller/seller-dashboard/seller-dashboard.component";
import { SellerProfileComponent } from "./seller/seller-profile/seller-profile.component";
import { HomeComponent } from "./home/home.component";
import { ProductsComponent } from "./seller/products/products.component";
import { PageNotFountComponent } from "./page-not-fount/page-not-fount.component";
import { SpinnerComponent } from './spinner/spinner.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AddProductComponent,
    UpdateProductComponent,
    FooterComponent,
    UsersComponent,
    SellersComponent,
    LoginComponent,
    DashboardComponent,
    SigninComponent,
    SignupComponent,
    ContactComponent,
    PrivacyComponent,
    TAndCComponent,
    OrdersComponent,
    UserDetailsComponent,
    SellerDetailsComponent,
    AllProductsComponent,
    ProductDetailsComponent,
    RegisterSellerComponent,
    SellerDashboardComponent,
    SellerProfileComponent,
    HomeComponent,
    ProductsComponent,
    PageNotFountComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    TagInputModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    MDBBootstrapModule.forRoot()
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [AngularFireAuth, AngularFireAuthModule, AngularFireModule],
  bootstrap: [AppComponent]
})
export class AppModule {}
