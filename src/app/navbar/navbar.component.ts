import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "../services/auth.service";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"]
})
export class NavbarComponent implements OnInit {
  constructor(private router: Router, private authService: AuthService) {}

  isLoggedIn: boolean;

  goToSignin() {
    try {
      this.router.navigate(["/signin"]);
    } catch (err) {
      console.log(err);
    }
  }

  goToSignup() {
    try {
      this.router.navigate(["/register-seller"]);
    } catch (err) {
      console.log(err);
    }
  }

  gotoDash() {
    try {
      this.authService.getAuth.subscribe(auth => {
        if (auth) {
          this.router.navigate(["seller/" + auth.uid]);
        }
      });
    } catch (err) {
      console.log(err);
    }
  }

  signOut() {
    this.authService.logout();
    this.router.navigate(["/"]);
  }

  ngOnInit() {
    this.authService.getAuth.subscribe(auth => {
      if (auth) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    });
  }
}
