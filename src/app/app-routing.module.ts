import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SigninSellerGaurdService } from "./services/gaurds/signin-seller-gaurd.service";

import { UpdateProductComponent } from "./seller/update-product/update-product.component";
import { AddProductComponent } from "./seller/add-product/add-product.component";
import { UsersComponent } from "./users/users.component";
import { SellersComponent } from "./sellers/sellers.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { AllProductsComponent } from "./all-products/all-products.component";
import { ContactComponent } from "./contact/contact.component";
import { OrdersComponent } from "./seller/orders/orders.component";
import { PrivacyComponent } from "./privacy/privacy.component";
import { ProductDetailsComponent } from "./seller/product-details/product-details.component";
import { SigninComponent } from "./signin/signin.component";
import { SignupComponent } from "./signup/signup.component";
import { TAndCComponent } from "./t-and-c/t-and-c.component";
import { UserDetailsComponent } from "./user-details/user-details.component";
import { RegisterSellerComponent } from "./seller/register-seller/register-seller.component";
import { SellerDashboardComponent } from "./seller/seller-dashboard/seller-dashboard.component";
import { SellerProfileComponent } from "./seller/seller-profile/seller-profile.component";
import { HomeComponent } from "./home/home.component";
import { PageNotFountComponent } from "./page-not-fount/page-not-fount.component";
import { ProductsComponent } from "./seller/products/products.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "dashboard", component: DashboardComponent },
  { path: "users", component: UsersComponent },
  { path: "sellers", component: SellersComponent },
  { path: "products", component: AllProductsComponent },
  { path: "contact", component: ContactComponent },
  { path: "orders", component: OrdersComponent },
  { path: "privacy", component: PrivacyComponent },
  { path: "product-details", component: ProductDetailsComponent },
  { path: "sellers", component: SellersComponent },
  { path: "signin", component: SigninComponent },
  { path: "signup", component: SignupComponent },
  { path: "terms-and-conditions", component: TAndCComponent },
  { path: "user-details", component: UserDetailsComponent },
  { path: "users", component: UsersComponent },
  { path: "register-seller", component: RegisterSellerComponent },
  { path: "404", component: PageNotFountComponent },
  {
    path: "seller/:id",
    component: SellerDashboardComponent,
    canActivate: [SigninSellerGaurdService]
  },
  {
    path: "seller/:id/profile",
    component: SellerProfileComponent,
    canActivate: [SigninSellerGaurdService]
  },
  {
    path: "seller/:id/add-product",
    component: AddProductComponent,
    canActivate: [SigninSellerGaurdService]
  },
  {
    path: "seller/:id/products",
    component: ProductsComponent,
    canActivate: [SigninSellerGaurdService]
  },
  {
    path: "seller/:id/orders",
    component: OrdersComponent
  },
  {
    path: "seller/:id/update-product:id1",
    component: UpdateProductComponent,
    canActivate: [SigninSellerGaurdService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
