import { Component, OnInit } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";

@Component({
  selector: "app-seller-profile",
  templateUrl: "./seller-profile.component.html",
  styleUrls: ["./seller-profile.component.scss"]
})
export class SellerProfileComponent implements OnInit {
  constructor(private afAuth: AngularFireAuth) {}

  // getUser() {
  //   this.afAuth.auth.onAuthStateChanged(user => {
  //     if (user) {
  //       console.log(true);
  //       this.seller = this.afAuth.auth.currentUser;
  //       console.log(this.seller.uid);
  //     }
  //   });
  // }

  ngOnInit() {
    // this.getUser();
  }
}
