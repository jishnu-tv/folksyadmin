import { Component, OnInit } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-seller-dashboard",
  templateUrl: "./seller-dashboard.component.html",
  styleUrls: ["./seller-dashboard.component.scss"]
})
export class SellerDashboardComponent implements OnInit {
  constructor(
    private afAuth: AngularFireAuth,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {}

  public chartType: string = "line";

  public chartDatasets: Array<any> = [{ data: [28, 48, 40, 19, 86, 27, 90] }];

  public chartLabels: Array<any> = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July"
  ];

  public chartColors: Array<any> = [
    {
      backgroundColor: "rgba(0, 137, 132, .2)",
      borderColor: "rgba(0, 10, 130, .7)",
      borderWidth: 2
    }
  ];

  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void {}
  public chartHovered(e: any): void {}

  elements: any = [
    { id: 1, first: "Mark", last: "Otto", handle: "@mdo" },
    { id: 2, first: "Jacob", last: "Thornton", handle: "@fat" },
    { id: 3, first: "Larry", last: "the Bird", handle: "@twitter" },
    { id: 4, first: "Larry", last: "the Bird", handle: "@twitter" },
    { id: 5, first: "Larry", last: "the Bird", handle: "@twitter" },
    { id: 6, first: "Larry", last: "the Bird", handle: "@twitter" },
    { id: 7, first: "Larry", last: "the Bird", handle: "@twitter" },
    { id: 8, first: "Larry", last: "the Bird", handle: "@twitter" },
    { id: 9, first: "Larry", last: "the Bird", handle: "@twitter" },
    { id: 10, first: "Larry", last: "the Bird", handle: "@twitter" }
  ];

  headElements = ["ID", "Product name", "Sold", "Stock"];

  user: any;
  id: string;
  spinner: boolean = true;

  getUser() {
    this.authService.getAuth.subscribe(auth => {
      if (auth) {
        this.id = auth.uid;
        console.log(this.id);
      }
    });

    this.afAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.user = this.afAuth.auth.currentUser;
      }
    });
  }

  addProduct() {
    this.router.navigate(["/seller/" + this.id + "/add-product"]);
  }

  viewProducts() {
    this.router.navigate(["/seller/" + this.id + "/products"]);
  }

  viewProfile() {
    this.router.navigate(["/seller/" + this.id + "/profile"]);
  }

  orders() {
    this.router.navigate(["/seller/" + this.id + "/orders"]);
  }

  ngOnInit() {
    this.authService.getAuth.subscribe(() => (this.spinner = false));
    this.getUser();
    try {
      this.authService.getAuth.subscribe(auth => {
        if (auth.uid != this.route.snapshot.params["id"]) {
          this.router.navigate(["/404"]);
        }
      });
    } catch (err) {
      console.log(err);
    }
  }
}
