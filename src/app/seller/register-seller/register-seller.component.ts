import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-register-seller",
  templateUrl: "./register-seller.component.html",
  styleUrls: ["./register-seller.component.scss"]
})
export class RegisterSellerComponent implements OnInit {
  constructor(private auth: AuthService) {}

  ngOnInit() {}

  createSeller(seller) {
    this.auth.createSeller(seller.value);
  }
}
