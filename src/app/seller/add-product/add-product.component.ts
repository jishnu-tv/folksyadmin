import { Component, OnInit } from "@angular/core";
import { Product } from "../../models/Product";
import { ProductService } from "../../services/product.service";
import { ActivatedRoute } from "@angular/router";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { AngularFireStorage } from "angularfire2/storage";
import { tap } from "rxjs/operators";

@Component({
  selector: "app-add-product",
  templateUrl: "./add-product.component.html",
  styleUrls: ["./add-product.component.scss"]
})
export class AddProductComponent implements OnInit {
  categories = ["Electronics", "Fasion", "Footware", "Home decor", "Books"];

  subCategories = ["Shirt", "T-Shirt", "Jeans", "Men", "Women"];

  uid: string = "";

  product: Product = {
    productName: "",
    shortName: "",
    longName: "",
    tags: "",
    category: "",
    subCategory: "",
    stock: 0,
    cod: false,
    onOffer: false,
    mrp: 0,
    currentPrice: 0,
    highlights: "",
    description: "",
    createdAt: new Date(),
    updatedAt: new Date(),
    sellerId: this.route.snapshot.params["id"]
  };

  images: File[] = [];
  image: File;
  imgsrc: any;

  constructor(
    private productservice: ProductService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private afStorage: AngularFireStorage
  ) {}

  selectFile(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.images.push(files.item(i));
    }
  }

  uploadImages() {
    const path = `images/products/${Date.now()}_${this.image.name}`;
    const ref = this.afStorage.ref(path);
    this.afStorage.upload(path, this.image);

    this.imgsrc = ref.getDownloadURL;
    console.log(this.imgsrc);
  }

  onSubmit() {
    this.uploadImages;
    this.productservice
      .addData(this.product)
      .then(() => {
        console.log("Form Submitted");
        this.product.productName = "";
        this.product.shortName = "";
        this.product.longName = "";
        this.product.tags = "";
        this.product.category = "";
        this.product.subCategory = "";
        this.product.stock = 0;
        this.product.cod = false;
        this.product.onOffer = false;
        this.product.mrp = 0;
        this.product.currentPrice = 0;
        this.product.highlights = "";
        this.product.description = "";
      })
      .catch(error => {
        console.log(error.message);
      });
  }

  ngOnInit() {
    try {
      this.authService.getAuth.subscribe(auth => {
        if (auth.uid != this.route.snapshot.params["id"]) {
          this.router.navigate(["/404"]);
        }
      });
    } catch (err) {
      console.log(err);
    }
  }
}
