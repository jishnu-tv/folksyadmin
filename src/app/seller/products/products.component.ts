import { Component, OnInit } from "@angular/core";
import { ProductService } from "../../services/product.service";
import { AuthService } from "../../services/auth.service";
import { AngularFireAuth } from "@angular/fire/auth";

@Component({
  selector: "app-products",
  templateUrl: "./products.component.html",
  styleUrls: ["./products.component.scss"]
})
export class ProductsComponent implements OnInit {
  constructor(
    private productService: ProductService,
    private authService: AuthService,
    private afAuth: AngularFireAuth
  ) {}

  getProducts() {
    this.authService.getAuth.subscribe(auth => {
      if (auth) {
        const id = auth.uid;
        console.log(id);
        let data = this.productService.getProducts(id);
        console.log(data);
      }
    });
  }

  ngOnInit() {
    this.getProducts();
  }
}
