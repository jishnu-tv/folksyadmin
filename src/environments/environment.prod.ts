export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyB7dukhVFwd66zIW5nb8CckQHWFgYR52Sk",
    authDomain: "folksy-89b43.firebaseapp.com",
    databaseURL: "https://folksy-89b43.firebaseio.com",
    projectId: "folksy-89b43",
    storageBucket: "folksy-89b43.appspot.com",
    messagingSenderId: "977755733418",
    appId: "1:977755733418:web:49ada999d289e099"
  }
};
